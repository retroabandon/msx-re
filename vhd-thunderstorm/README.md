Thunderstom Windows-controlled VHD Mod
======================================

The page [VHD I/Fなしでサンダーストームを動かすやつ][th] ("Running
Thunderstorm Without the VHD Interface") discusses how to modify the
software to control a Windows machine with a copy of the video from the
game's [VHD][] (Video High Density) disc.

The user is responsible for figuring out how to rip the video from the VHD
disc, but once it's been ripped there are instructions for verifying the
recording and aligning the start of the VHD file.

The MSX system requires:
- a superimposer (to mix the VHD video and MSX graphics);
- a floppy disk subsystem (the mod runs only from floppy);
- copies of files from the _Thunderstorm_ tape saved to a floppy;
- 48K RAM (page 1 RAM is used by the modification);
- RS-232 interface and crossover cable (to communicate with the Windows
  software); and
- a Windows machine with a serial port and VLC media player.

The original game tape appears to contain two files:
- `THUNDER.BAS`: Loader for the following file; modified by `DIFF.ASC` below.
- `STORM.BIN`: The game itself. It loads 7 KB above the start of BASIC at
  $8700-$DFFF and the entry point is $A083.

The modification must be run from disk (with MSX disk BASIC booted). The
[archive][th-zip] linked from the above page contains the following files:
- `DIFF.ASC`: A patch for `THUNDER.BAS`. (What exactly it does and why is
  not yet known.)
- `LOAD.BIN`: A binary file with an MSX-BASIC `LOAD` header that loads
  `VHD.BIN` from disk into the RAM assigned to page 1 and sets it up to
  look like a cartridge with BASIC statement expansion code. It's not yet
  clear how or when this is executed.
- `VHD.BIN`: (Not yet disassembled. ROM header information below.)
- `thunderstorm.exe`: A Windows application that presumably accepts
  commands from the MSX machine via the serial port and passes them on to
  VLC media player.

### Loader Changes

`DIFF.ASC`, reformatted for clarity and comments added:

     1 IF PEEK(&hF673) < &hE1       ‖ MEMSIZ+1 (MSB); mem free to $E100?
          THEN PRINT "NO ENOUGH MEMORY."
             : END
    30 BLOAD "STORM"                ‖ loads to $8700 - $DFFF
    31 DEFUSR=&hFD9F                ‖ H.TIMI, timer interrupt handler
     : FOR A = 0 TO 255
     :   B=USR(0)
     : NEXT A
    32 POKE &hB39F, &h50+&B1100     ‖ B39F: 5C
    33 POKE &hB3A7, &h50+&B1100     ‖ B3A7: 5C
    34 POKE &hB3CC, &hD0+&B1100     ‖ B3CC: DC
    35 POKE &hB3D1, &h90+&B1100     ‖ B3D1: 9C
    38 POKE &hDE82, &hEC            ‖ DE82: EC
     : POKE &hDE83, &h88            ‖ DE83: 88
    39 DEFUSR=&hA083 : A=USR(0)     ‖ call STORM.BIN entry point

It's not clear what the purpose of calling `H.TIMI` 256 times is.

The `POKE`s produce the following. Why he express $5C as $50 + $0C =
$50 + %1100 is not clear. The opcodes given in the comments are just hints;
there's no evidence yet that these are not instead data values.

    B39F: 5C    ; ld e,h
    B3A7: 5C    ; ld e,h
    B3CC: DC    ; call c,nn
    B3D1: 9C    ; sbc a,h
    DE82: EC    ; call pe,nn
    DE83: 88    ; adc a,b

### VHD.BIN

`VHD.BIN`: len=$4000, $FF fill from $012D to end. Starts with a cartridge
[cartridge ROM header][cart-ab] whose entry addresses make it clear the
cart starts at $4000 (page 1):

    0000: 41 42     'AB'    ROM signature
    0002: 10 40     $4010   INIT: called by BIOS on startup
    0004: 47 40     $4047   STATEMENT: MSX BASIC invokes for expansion
    0006: 00 00      -      DEVICE: 0000=not present
    0008: 00 00      -      BASIC program text to run at boot; 0000=not present
    000A: 00 00 00  reserved
    000D: 00 00 00



<!-------------------------------------------------------------------->
[VHD]: https://en.wikipedia.org/wiki/Video_High_Density
[cart-ab]: https://github.com/0cjs/sedoc/blob/master/8bit/msx/rom.md#rom-header
[th-zip]: http://d4.princess.ne.jp/download/dl.cgi?thunderstorm.zip
[th]: http://d4.princess.ne.jp/msx/gomi/thunderstorm.htm
