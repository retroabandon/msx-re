SCC+ Sound Cartridge
====================

The [SCC+ Sound Cartridge][scc+] is a reproduction of the cartridge
originally supplied with the SD-SNATCHER game but with 2048 KB of
memory instead of 64 KB.

The `SCROM.COM` program loads a ROM image into the cartridge RAM and lets
you play it.


<!-------------------------------------------------------------------->
[scc+]: https://www.ebsoft.fr/shop/en/home/96-scc-sound-cartridge.html
