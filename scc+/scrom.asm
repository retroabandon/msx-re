; SCROM v1.3 (v1.2 modified by gdx to support Roms up to 2048kB)
; ROM loader for Sound Cartridge

            cpu z80
            relaxed on

; ----------------------------------------------------------------------
;   MSX definitions  (DI=disables interrupts)

ENASLT      equ  $024       ; DI map A=slotdesc to page containing addr HL

RAMAD1      equ  $F342      ; slotdesc of RAM for page 1 (DOS)
RAMAD2      equ  $F343      ; slotdesc of RAM for page 2 (DOS)
EXPTBL      equ  $FCC1      ; slotdesc table for pages 0-3
H.STKE      equ  $FEDA      ; hook to start ROM after disk bufs alloc'd

; ----------------------------------------------------------------------

SC_128      EQU  0
SC_SD       EQU  1
SC_TH       EQU  2

;           aseg

            org 100h

start:
            ld   de,INTROMSG
            ld   c,9
            call 5              ; Print the title

            call findsc         ; sets `scslot` to cart slotdesc or $FF
            call gettype
            call printsc

            call get_opts

            ld   a,(scslot)
            inc  a              ; is slotdesc of card $FF = not found?
            jp   z,eop          ;   yes: just exit (without an error)

            ld   a,(options)
            or   a
            ld   hl,5ch
            jr   z,parm1
            ld   hl,6ch
parm1:
            ld   de,FCB
            ld   bc,1+8+3
            ldir

            ld   de,FCB
            ld   c,0fh
            call 5
            ld   de,FILEOPEN
            or   a
            jp   nz,fail

            ld   hl,(FCB+17)
            ld   b,3
next_rot:
            sla  l
            rl   h
            djnz next_rot
            ld   a,h
            ld   (size),a
            cp   9
            jr   c,ok
            cp   17
            jr   ok              ; nc,notok
            ld   a,(type)
            cp   SC_128
            jr   z,ok
notok:
            ld   de,TOSMALL
            ld   c,9
            call 5
            ; fallthrough? or error exit?

ok          call loadrom
            ld   a,(options)
            or   a
            ld   de,FILEOK
            jr   z,fail

            ld   hl,5eh
nxt_opt1    ld   a,(hl)
            inc  hl
            cp   'c'
            jr   z,doconv
            cp   'C'
            jr   z,doconv
            cp   'a'
            jr   z,doconva
            cp   'A'
            jr   z,doconva
            cp   ' '
            jr   z,rest_opt
            jr   nxt_opt1

doconv      call convert
            jr   rest_opt

doconva     call converta

rest_opt:
            ld   hl,5eh
next_opt:
            ld   a,(hl)
            inc  hl
            cp   'r'
            jp   z,reset
            cp   'R'
            jp   z,reset
            cp   's'
            jp   z,dostart
            cp   'S'
            jp   z,dostart
            cp   ' '
            ld   de,FILEOK
            jp   z,fail
            jr   next_opt

fail        ld   c,9
            call 5
            call resetsc
            ; fallthrough

;   End of Program: restore DOS RAM pages and exit.
eop         ld   a,(RAMAD1)     ; slotdesc of RAM for page 1 (DOS)
            ld   h,$40          ; page 1
            call ENASLT
            ld   a,(RAMAD2)
            ld   h,$80          ; page 2
            call ENASLT
            rst  0

loadrom     ld   de,LOAD
            ld   c,9
            call 5              ; Print "Loading..."

            ld   hl,$2000
            ld   (FCB+14),hl
            ld   hl,0
            ld   (FCB+33),hl
            ld   (FCB+35),hl

            ld   de,$2000
            ld   c,$1A
            call 5              ; Disk Transfer Address = 2000h

            ld   a,(type)
            ld   bc,$0808
            cp   SC_SD
            jr   z,ok_blok
            ld   bc,$0800
            cp   SC_TH
            jr   z,ok_blok
            ld   bc,$FF00       ; 01000h

ok_blok     ld   a,(size)
            cp   b
            jr   nc,nxt_blok
            ld   b,a
nxt_blok:
            push bc

            ld   de,FCB
            ld   hl,1
            ld   c,27h
            call 5
            or   a
            ld   de,FILEREAD
            jr   nz,fail

            ld   a,(first)
            or   a
            jr   nz,notchek
            ld   hl,2000h
            ld   d,(hl)
            inc  hl
            ld   e,(hl)
            ld   hl,04142h      ; 'AB'
            call rst_20h
            ld   de,FILEWONG
            ld   c,9
            call nz,5
            ld   a,1
            ld   (first),a

notchek     ld   a,(scslot)     ; map page 2 to our cartridge RAM
            ld   h,$80
            call ENASLT
            ;
            xor  a
            ld   ($BFFE),a
            pop  bc
            push bc
            ld   a,c
            ld   ($9000),a
            ld   a,24h          ; 1fh
            ld   ($BFFE),a

            ld   hl,$2000
            ld   de,$8000
            ld   bc,$2000
            ldir

            pop  bc
            inc  c
            djnz nxt_blok
            ret

resetsc     ld   a,(scslot)
            ld   h,$40          ; page 1
            call ENASLT
            ld   a,(scslot)
            ld   h,$80          ; page 2
            call ENASLT

            xor  a
            ld   ($BFFE),a
            ld   c,a
            ld   a,(type)
            cp   SC_SD
            jr   nz,not_sd
            ld   c,8
not_sd      ld   a,c
            ld   ($5000),a
            inc  a
            ld   ($7000),a
            inc  a
            ld   ($9000),a
            inc  a
            ld   ($B000),a
            ret

reset:
            call resetsc
            call stop_it

            ld   a,(EXPTBL)
            ld   hl,0
            push hl
            jp   024h

dostart:
            call stop_it
            call resetsc

            ld   hl,H.STKE
            push hl
            ld   hl,(4002h)
            push hl
            ld   a,(EXPTBL)
            ld   h,0
            jp   024h

findsc:
            ld   bc,$0400           ; count, current primary slot number
            ld   hl,EXPTBL
nxtprim:
            push bc
            push hl
            ld   a,(hl)             ; slotdesc for current slot
            bit  7,a
            jr   nz,expanded
            ld   a,c                ; primary slot number
            call chkslot
            jr   not_exp
expanded:
            call chk_exp
not_exp:
            pop  hl
            pop  bc
            ret  c                  ; carry set? we found it so return
            inc  hl
            inc  c
            djnz nxtprim
            ld   a,$FF              ; cart not found: note error
            ld   (scslot),a
            ret

chk_exp:
            and  80h
            or   c
            ld   b,4
next_exp:
            push bc
            call chkslot
            pop  bc
            ret  c
            add  a,4
            djnz next_exp
            ret

chkslot     ld   (scslot),a         ; save primary slot number
            ld   h,80h              ; map page 2 to slotdesc in A
            call ENASLT
            call set_vals
            call chk_vals
            ld   a,(scslot)         ; restore primary slot number
            ret                     ; return w/carry set if found

set_vals
            ld   bc,1000h
nxt_val:
            push bc
            call set_page
            ld   l,c
            ld   h,c
            ld   (9000h),hl
            ld   (9002h),hl
            pop  bc
            inc  c
            djnz nxt_val
            xor  a
            ld   ($BFFE),a
            ret

set_page:
            xor  a
            ld   ($BFFE),a
            ld   a,c
            ld   (09000h),a
            ld   a,24h          ; 1fh
            ld   ($BFFE),a
            ret

chk_vals:
            ld   a,20h
            ld   ($BFFE),a
            ld   bc,0808h
next_val:
            ld   a,c
            ld   (9000h),a
            call compare
            jr   nc, chk_snat
            inc  c
            djnz next_val
            scf
            ret

chk_snat:
            ld   bc,0800h
nextval2:
            ld   a,c
            ld   (9000h),a
            call compare
            ret  nc
            inc  c
            djnz nextval2
            scf
            ret

compare:
            ld   hl,(9000h)
            call sub_comp
            ret  nc
            ld   hl,(9002h)
sub_comp:
            ld   a,l
            cp   h
            jr   nz,noway
            cp   c
            jr   nz,noway
            scf
            ret
noway:
            and  a
            ret

gettype:
            ld   a,(scslot)
            ld   h,80h
            call 024h

            ld   bc,0FF00h      ; 01000h
            ld   hl,9000h
            ld   de,9001h

nxt_val2:
            xor  a
            ld   ($BFFE),a
            ld   a,c
            ld   (hl),a
            ld   a,24h          ; 1fh
            ld   ($BFFE),a
            ld   a,c
            ld   (de),a
            inc  c
            djnz nxt_val2

            xor  a
            ld   ($BFFE),a
            ld   (hl),a
            ld   a,(de)
            cp   8
            ld   c,SC_TH
            jr   z,found
            cp   255
            ld   c,SC_SD
            jr   z,found
            ld   (hl),8
            ld   a,(de)
            cp   255
            ld   c,SC_TH
            jr   z,found
            ld   c,SC_128
found:
            ld   a,c
            ld   (type),a
            ret

convert:
            ld   a,(scslot)
            ld   h,80h
            call 024h

            ld   a,(type)
            ld   bc,0808h
            cp   SC_SD
            jr   z,nextblok
            ld   bc,0800h
            cp   SC_TH
            jr   z,nextblok
            ld   bc,0FF00h      ; 1000h
nextblok:
            push bc
            xor  a
            ld   ($BFFE),a
            ld   a,c
            ld   (09000h),a
            inc  a
            ld   (0b000h),a
            ld   a,24h          ; 1fh
            ld   ($BFFE),a
            ld   hl,8000h
next_ad:
            ld   a,(hl)
            cp   32h
            jr   nz,next
            inc  hl
            ld   a,(hl)
            or   a
            jr   nz,next
            inc  hl
            ld   a,(hl)
            cp   040h
            jr   z,ad_ok
            cp   060h
            jr   z,ad_ok
            cp   080h
            jr   z,ad_ok
            cp   0A0h
            jr   nz,next
ad_ok:
            add  a,10h
            ld   (hl),a
next:
            inc  hl
            ld   a,h
            cp   0a0h
            jr   nz,next_ad
            pop  bc
            inc  c
            djnz nextblok
            xor  a
            ld   ($BFFE),a
            ld   a,(RAMAD2)
            ld   h,80h
            jp   024h

converta:
            ld   a,(scslot)
            ld   h,80h
            call 024h

            ld   a,(type)
            ld   bc,0808h
            cp   SC_SD
            jr   z,nxtbloka
            ld   bc,0800h
            cp   SC_TH
            jr   z,nxtbloka
            ld   bc,1000h
nxtbloka:
            push bc
            xor  a
            ld   ($BFFE),a
            ld   a,c
            ld   (09000h),a
            inc  a
            ld   (0b000h),a
            ld   a,24h          ; 1fh
            ld   ($BFFE),a
            ld   hl,8000h
next_ada:
            ld   a,(hl)
            cp   32h
            jr   nz,nexta
            inc  hl
            ld   a,(hl)
            or   a
            jr   nz,nexta
            inc  hl
            ld   a,(hl)
            cp   060h
            ld   c,50h
            jr   z,ad_oka
            cp   068h
            ld   c,70h
            jr   z,ad_oka
            cp   070h
            ld   c,90h
            jr   z,ad_oka
            cp   078h
            ld   c,0b0h
            jr   nz,nexta
ad_oka:
            ld   (hl),c
nexta:
            inc  hl
            ld   a,h
            cp   0a0h
            jr   nz,next_ada
            pop  bc
            inc  c
            djnz nxtbloka
            xor  a
            ld   ($BFFE),a
            ld   a,(RAMAD2)
            ld   h,80h
            jp   024h

rst_20h:
            ld   a,d
            cp   h
            ret  nz
            ld   a,e
            cp   l
            ret

printsc:
            ld   a,(scslot)
            cp   255
            ld   de,NOSC
            ld   c,9
            jp   z,5
            and  3
            add  a,'0'
            ld   (primslot),a
            ld   a,(scslot)
            rra
            rra
            and  3
            add  a,'0'
            ld   (secslot),a
            ld   de,SCFOUND
            ld   c,9
            call 5
            ld   a,(type)
            ld   de,type128
            cp   SC_128
            jr   z,go
            ld   de,typeSD
            cp   SC_SD
            jr   z,go
            ld   de,typeTH
go:         ld   c,9
            jp   5

get_opts:
            ld   a,(5dh)
            cp   ' '
            ld   de,USAGE
            jp   z,fail
            cp   '-'
            ld   a,0
            jr   nz,noopt
            ld   a,(6dh)
            cp   ' '
            ld   de,USAGE
            jp   z,fail
            ld   a,1
noopt:
            ld   (options),a
            ret

stop_it:
            ld   b,0
nextstop:
            push bc
            call 0fd9fh
            pop  bc
            djnz nextstop
            ret

INTROMSG:   db   'SCROM: Sound Cartridge (SCC+) ROM loader',13,10
            db   'By Sean Young, 1997          version 1.3',13,10,13,10,'$'
NOSC:       db   'No Sound Cartridge found!',13,10,'$'
SCFOUND:    db   'Sound Cartridge found in slot '
primslot:   db   '0-'
secslot:    db   '0.',13,10,'$'

type128:    db   'Sound Cartridge with more RAM ',13,10,13,10,'$'
typeTH:     db   'The Snatcher Sound Cartridge (64Kb RAM)',13,10,13,10,'$'
typeSD:     db   'SD Snatcher Sound Cartridge (64Kb RAM)',13,10,13,10,'$'

TOSMALL:    db   'Warning: File larger than memory in cartridge.',13,10,13,10,'$'
FILEWONG:   db   'Warning: File not in ROM format.',13,10,13,10,'$'

FILEOPEN:   db   'Cannot open file!$'
FILEOK:     db   'File loaded.$'
FILEREAD:   db   'Unable to read file!$'
LOAD:       db   'Loading...',13,10,'$'

USAGE:      db   'Loads a ROM file into the RAM of a Sound Cartridge.',13,10,13,10
            db   'SCROM [-<options>] <romfile>',13,10
            db   '  <options>',13,10
            db   '     c   Loads konami4 format file (converts to konami5).',13,10
            db   '     a   Loads ascii format file (converts to Ascii/8Kb).',13,10
            db   '     r   Resets the MSX after loading the ROM file.',13,10
            db   '     s   Starts the ROM file after loading it.',13,10
            db   '  <romfile>',13,10
            db   '     filename and extention of the ROM file to load.',13,10,'$'

scslot:     db   0          ; slotdesc of the cart or $FF if not found
type:       db   0
options:    db   0
size:       db   0
first:      db   0
FCB:        db   37 dup 0

            END  ; start
