MSX Documentation
-----------------

The following are contained in this directory:
- [`msx2-th-en`]: ASCII's _MSX2 Technical Reference Manual_ in an English,
  ASCII text-only version, from archive.org [MSX2 Technical Handbook
  [MSX]][msx2-txt-en]. The formatting of tables is maintained pretty well.
  Note that there is another version of this linked below.

The following additional references may be useful:
- \[td1] [_MSX Technical Data Book_][td1] (en), Sony, 1984.
  MSX1 hardwre and software.
- \[yts] Yamaha Computer [_MSX Series Technical Summary_][yts] (en)
  (GB, for series AX-100, YIS-503F, CX-5M)
- \[tdR] [_MSX turbo Rテクニカル・ハンドブック_][tdR] (ja)
- \[mdp] [_MSX-Datapack_][mdp] (ja), 株式会社アスキー. MSX2+, MSX2, some MSX1.
  Large, detailed referenced published by ASCII.
  Possibly the best reference available; includes many peripherals.
  Wiki copy at above link, original scans:
  - [Volume 1][mdp-v1]: 1 Hardware. 2 System Software. 3 MSX-DOS.
    4 VDP (V9938 and V9958).
  - [Volume 2][mdp-v2]: 5 Slots and Cartridges. 6 Standard Peripherals.
    7 Optional Peripherals. Appendix BIOS, Work Area, etc.
- \[2thj-fe] [MSX2テクニカル・ハンドブック][2thj-fe], 1st ed. (ja),
  アスキー版局, 1986. Poor scan, poor OCR.
- \[2thj] [MSX2テクニカル・ハンドブック][2thj], 7th ed. (ja),
  アスキー版局, 1988. Better scan and OCR, though OCR still not perfect.
- \[2the] [_MSX2 Technical Handbook_][2the] (en). Trans. of above. Markdown.
- \[rbr] [_The Revised MSX Red Book_] (en), Avalon Software, 1985.
  Based on [[mdp]], [[2thj1]], MSX Magazine.
  This version revised by by Cyberknight Masao Kawata 1998-2005.


<!-------------------------------------------------------------------->

<!-- this directory -->
[`msx2-th-en`]: ./msx2-th-en/
[msx2-text-en]: https://archive.org/details/MSX-MSX2TechnicalHandbook

<!-- Technical books -->
[2the]: https://github.com/Konamiman/MSX2-Technical-Handbook/
[2thj-fe]: https://archive.org/details/MSX2TechnicalHandBookFE1986/
[2thj]: https://archive.org/details/Msx2TechnicalHandBook/
[mdp-v1]: https://archive.org/details/MSXDatapackVolume1
[mdp-v2]: https://archive.org/details/MSXDatapackVolume2
[mdp-v3]: https://archive.org/details/MSXDatapackVolume31991OCRTabajara
[mdp]: http://ngs.no.coocan.jp/doc/wiki.cgi/datapack
[rbr]: https://www.angelfire.com/art2/unicorndreams/msx/RR-Intro.html
[td1]: https://archive.org/stream/MSXTechnicalHandbookBySony#page/n5/mode/1up
[tdR]: https://archive.org/details/MsxTurboR
[yts]: https://archive.org/details/yamahacx5myis503ts/mode/1up
