OLD/UNNEW Program from RAM #84
==============================

This is a program that supplies the `OLD` command, which restores a BASIC
program after `NEW` has been typed. It's from [page 23][r23] of the Dutch
magazine _RAM (Computer & Radio Amateur Magazine)_ issue #84, 1997-11.

![Bottom part of page 23](page-23-bot.jpeg)

### Files

- `ram-old.baa`: ASCII save (with `,A` argument) of the typed-in BASIC
  program.
- `ram-old.bin`: `BLOAD`-format binary version of the program.
- `ram-old.z80`: Reverse-engineered source for the binary program.
- `compare`: Quick-and-dirty script to build the source with [Macro
  Assembler AS][maas] and compare the output to the original binary.



<!-------------------------------------------------------------------->
[maas]: http://john.ccac.rwth-aachen.de:8000/as/
[r23]: https://archive.org/details/radio_amateur_magazine/RAM_084_1987_11/page/23/mode/1up?view=theater
