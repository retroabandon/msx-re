
            cpu  Z80
            relaxed on

; ----------------------------------------------------------------------

READYR      equ   $409B         ; BASIC main loop
LINKER      equ   $4253         ; rebuilds BASIC text line link fields
SCRTCH      equ   $6287         ; initialise vars and interpreter

;   BASIC I/O buffer storage re-allocation, or something like that.
;   • https://www.angelfire.com/art2/unicorndreams/msx/RR-BASIC.html
IOALLOC     equ   $7E6B

TXTTAB      equ   $F676
HIMEM       equ   $FC4A

;   BASIC hook for ???. From msxsyssrc, appears to be called after
;   getting the next character on a BASIC line during statement execution.
;   • https://archive.org/details/MSXTechnicalHandbookBySony/page/n159/mode/1up
H.GONE      equ   $FF43
;   All hooks are five bytes long.
hooklen     equ   5

; ----------------------------------------------------------------------

            ;   This is in KBUF ($F41F-$F55C), the crunch buffer
            ;   used by BASIC to tokenise input lines.
            org   $F500

init        ;   Reserve space for stay-resident routine.
            ld   hl,(HIMEM)
            ld   de,2+end-oldcmd ; ??? length of resident routine + 2 (why +2?)
            or   a              ; reset carry flag
            sbc  hl,de          ; ??? why not just use SUB?
            ld   (HIMEM),hl     ; move HIMEM down to make room
            ld   a,1            ; ???
            call IOALLOC        ; ??? re-allocate buffers?

            ;   Set up H.GONE hook to call our resident code
            ld   hl,H.GONE      ; copy old hook handler
            ld   de,nexthook    ;   to where we will call it after our hook
            ld   bc,hooklen
            ldir
            ld   hl,(HIMEM)     ; insert address of our resident routine
            ld   (H.GONE+1),hl
            ld   a,$C3          ; prefix it with a JMP instruction
            ld   (H.GONE),a

            ;   Copy resident routine to the space we've reserved
            ex   de,hl          ; dest: HIMEM
            ld   hl,oldcmd      ; source: routine below
            ld   bc,end-oldcmd  ; length of routine
            ldir                ; copy

            ;   Restart BASIC main loop.
            ;   Not clear why we don't just RET. This will stop a running
            ;   program if it was called from within a program via
            ;   BLOAD "...",R or whatever.
            jp   READYR         ; restart BASIC main loop

; ----------------------------------------------------------------------

;   Resident handler, living at HIMEM, for the `OLD` command.
;   This is called via the H.GONE hook.
;   WARNING: This routine must be relocatable!
oldcmd      cp   'O'            ; start of 'OLD'?
            jr   nz,nexthook    ;    no: continue with old H.GONE hook
            push hl             ; save char ptr for not_old
            push af             ; save current char for not_old
            inc  hl             ; next char
            ld   a,(hl)
            cp   'L'
            jr   nz,+           ; skip increment if it's not 'L'?
                                ;   (we accept 'OD', too, for some reason?)
            inc  hl
+           ld   a,(hl)
            cp   'D'            ; is it our command?
            jr   nz,not_old     ;    no: let basic carry on
            ld   hl,(TXTTAB)    ; will be $0000 after NEW
            ;   Make the first line pointer $0001, causing LINKER to see this
            ;   as an invaid line address, rather than a valid end of program.
            inc  (hl)
            call LINKER         ; recreate all BASIC line link fields
            ;   Call SCRTCH to re-initialise the interpeter, but skip the
            ;   initial LD HL,(TXTTAB) so that the HL it uses is the value
            ;   returned by LINKER, rather than the start of the BASIC program,
            ;   so that we re-init keep the existing program when we re-init.
            call SCRTCH+3
            jp   READYR         ; restart BASIC main loop

            ;   We parsed something other than 'OLD' or 'OD', so restore
            ;   the original parse position and char and let the next hook
            ;   try to parse it.
not_old     pop  af             ; restore current char
            pop  hl             ; restore pointer to line BASIC is parsing
            ; fallthrough

            ; The `init` routine will replace this with the code from the
            ; previous hook in H.GONE when we initialised.
nexthook    ret
            ds   hooklen-1
end                             ; for calculating size of this resident routine
